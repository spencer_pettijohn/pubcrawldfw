package com.chaossecurity.pubcrawlDFW;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.utils.demo.model.Bars;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lk on 11/17/14.
 */
public class FtWorthActivity extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : " + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address + ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.754073, -97.329595), 15f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        // http://www.flickr.com/photos/nypl/3111525394/
        mClusterManager.addItem(new Bars(position(32.753763, -97.331334), "Avalan", "515 Houston StFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.751447, -97.330477), "Bar 9", "900 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7304394, -97.3326419), "Brewed", "801 W Magnolia aveFort Worth  TX  76104 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7532999, -97.3310504), "Durty Murphys", "609 Houston StFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.75245, -97.328005), "Embargo", "210 E 8th St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7554537, -97.331255), "Flying Saucer - Ft Worth", "111 E 3rd St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7547884, -97.3326198), "Four Day Weekend", "312 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7534488, -97.331453), "Fox & Hound - Fort Worth", "603 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7536605, -97.3342971), "Frankies Sports Bar - Ft Worth", "425 W 3rd St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.749059, -97.327881), "Ft Worth Convention Center", "1201 Houston StreetFt Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.751387, -97.330437), "Houston St. Bar & Patio", "902 Houston St Fort Worth  TX 76101 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753363, -97.3313969), "Hyenas Comedy Club - Ft Worth", "605 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7606132, -97.3433936), "J&J Blues Bar", "937 Woodward StFort Worth TX 76107 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753073, -97.3310328), "Library Bar", "611 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7552507, -97.3303781), "Lone Star - Sundance", "425 Commerce St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7501817, -97.3258838), "Malones Pub", "1303 Calhoun St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.750333, -97.3298219), "Mambos Cantina - Ft Worth", "1010 Houston StreetFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753763, -97.331334), "Mosaic Lounge", "515 Houston StFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753763, -97.331335), "Ojos Locos", "515 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753763, -97.331334), "Paddy Reds Irish Pub", "903 Throckmorton Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.753157, -97.331104), "Petes Dueling Piano Bar - Ft Worth", "621 Houston St Fort Worth TX 76102 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(32.753763, -97.331334), "Red Goose Saloon", "306 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.751514, -97.3301617), "Rick OSheas Pub", "904 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7540118, -97.3315537), "Scat Jazz Lounge", "111 W 4th St Fort Worth TX 76102 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(32.7458497, -97.3267889), "T&P Tavern", "221 W Lancaster Ave Ste 1000 Fort Worth  TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7485306, -97.328477), "The Water Horse - Omni", "1300 Houston StreetFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7532273, -97.3333942), "Tower Restaurant Speakeasy", "525 Taylor St.Fort Worth  TX  76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.752801, -97.3333634), "Vee Lounge", "500 Taylor StFort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7532064, -97.3330012), "Vice", "350 W 5th St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7485306, -97.328477), "Whiskey & Rye", "1300 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7485306, -97.328477), "Wine Thief", "1300 Houston St Fort Worth TX 76102 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7511146, -97.3302129), "Zambrano Wine Cellar", "910 Houston St Ste 110 Fort Worth TX 76102 " , R.drawable.dance));



    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }
}
