package com.chaossecurity.pubcrawlDFW;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.chaossecurity.pubcrawlDFW.Bars;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lk on 11/17/14.
 */
public class UptownActivity extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : "  + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address+ ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.799052, -96.803466), 15f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        // http://www.flickr.com/photos/nypl/3111525394/
        mClusterManager.addItem(new Bars(position(32.7993894, -96.805657), "6th Street Bar", "3005 Routh StDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.800067, -96.800438), "Avenu Lounge", "2912 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.81655, -96.7935516), "BJs NXS", "3215 Fitzhugh Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.797317, -96.801882), "Black Friar Pub", "2621 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8223776, -96.7906126), "Bodega Wine Bar", "4514 Travis St Dallas TX 75205 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7983813, -96.8052426), "Concrete Cowboy", "2512 Cedar Springs RdDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8071323, -96.7962219), "Cork Wine Bar", "3636 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8076605, -96.7979099), "Cru, A Wine Bar - Uptown", "3699 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7943306, -96.8014146), "Dragonfly at Hotel Zaza", "2332 Leonard StDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7976539, -96.7983839), "Four Lounge", "2418 Allen St Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8040117, -96.7999), "Frankies Sports Bar - Uptown", "3227 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7982743, -96.8029609), "Ginger Man - Uptown", "2718 Boll St Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7891681, -96.8043878), "Glass Lounge", "1899 McKinney Ave Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8012646, -96.8071065), "Katy Trail Ice House", "3136 Routh St Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8209638, -96.7887146), "Knox Street Pub & Grill", "4447 Mckinney Ave Dallas TX 75205 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7989611, -96.8051459), "Kung Fu", "2911 Routh St.Dallas TX  75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.820089, -96.784255), "Lemon Bar - Dallas", "2822 N Henderson AveDallas TX 75206 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.799711, -96.800634), "Lotus Bar", "2900 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.799284, -96.800578), "Mckinney Avenue Tavern", "2822 Mckinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7989788, -96.8034439), "Momos Lounge", "2800 Routh St #165Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.79656, -96.796815), "Nodding Donkey", "2900 Thomas Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.805337, -96.799081), "Patio Grill", "3403 McKinney AvenueDallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7987799, -96.8051779), "Primebar", "2520 Cedar Springs RdDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.792433, -96.803742), "Rattlesnake Bar", "2121 McKinney Ave Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7963549, -96.8022524), "Renfields Corner", "2603A Routh St Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7891681, -96.8043877), "Revive Uptown", "1899 McKinney AveDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7891681, -96.8043875), "Rio Room", "4515 Travis St Dallas TX 75205 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7891681, -96.8043877), "Sharaku Sake Lounge", "2633 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7950431, -96.8035025), "Sisu Uptown", "2508 MAPLE AVENUE Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7975245, -96.8026329), "Social House Uptown", "2708 Routh Street Dallas Dallas Texas  75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8000535, -96.8006137), "St. Johns Wood", "2908 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.78046, -96.801215), "Synn Nightclub", "1217 Main StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(35.3658427, -84.6736908), "Teddys Room", "2404 Cedar Springs Rd. #400Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7979211, -96.798046), "The Alcove", "2907 State St Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7981912, -96.8062296), "The Common Table", "2917 Fairmount St.Dallas TX  75210 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.797766, -96.800977), "The Den", "2710 McKinney AveDallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.796975, -96.801244), "The Idle Rich Pub", "2614 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.796975, -96.801244), "The Londoner - Uptown", "2909 Thomas Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8067476, -96.7978097), "The Loon", "3531 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7942997, -96.7969836), "The Mason Bar", "2701 Guillot StDallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8213808, -96.7871735), "The Monroe", "3001 Knox St Dallas TX 75205 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8047006, -96.7994775), "The Quarter Bar", "3301 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7979961, -96.8009697), "The Trophy Room", "2714 McKinney Ave Dallas TX 75204 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7998859, -96.8010817), "Vino 100", "2909 McKinney Ave Dallas TX 75204 " , R.drawable.dance));




    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }}