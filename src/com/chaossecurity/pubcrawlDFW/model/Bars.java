package com.chaossecurity.pubcrawlDFW..model;

/**
 * Created by lk on 11/17/14.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class Bars implements ClusterItem {
    public final String name;
    public final String address;
    public final int profilePhoto;
    private final LatLng mPosition;

    public Bars(LatLng position, String name, String address, int pictureResource) {
        this.name = name;
        this.address = address;
        profilePhoto = pictureResource;
        mPosition = position;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
