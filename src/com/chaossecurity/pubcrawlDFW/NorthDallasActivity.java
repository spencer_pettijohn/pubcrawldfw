package com.google.maps.android.utils.demo;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.chaossecurity.pubcrawlDFW.model.Bars;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lk on 11/18/14.
 */
public class NorthDallasActivity extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : "  + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address+ ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(33.013330, -96.828984), 12f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        mClusterManager.addItem(new Bars(position(32.955075, -96.85044), "300 Dallas", "3805 Belt Line Rd Addison TX 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9536629, -96.826628), "Addison Improv Comedy Club", "4980 Beltline Rd Dallas TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9535728, -96.835834), "BlackFinn American Saloon - Addison", "4440 Belt Line Rd Addison TX 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9528147, -96.8189421), "Flying Saucer - Addison", "14999 Montfort Dr Dallas TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9538393, -96.8445996), "McFaddens", "4050 Beltline RdAddison Texas 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9511085, -96.8190595), "Mercy Wine Bar", "5100 Belt Line Rd Dallas TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9540096, -96.8266775), "Petes Dueling Piano Bar - Addison", "4980 Belt Line Rd Dallas TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9536987, -96.8266796), "Red Room Addison", "4950 Belt Line Road Suite 180Dallas  TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9511085, -96.8190595), "Sherlocks Baker Street Pub & Grill - Addison", "5100 Belt Line Rd Addison TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9540177, -96.8289033), "Stadium Cafe", "4872 Belt Line Rd Addison TX 75254 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.953793, -96.844092), "The Back 9", "4060 Beltline Rd Addison  TX  75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9548728, -96.8401564), "The Hub Sports Bar and Grill", "4145 Beltline Rd. Addison Tx  75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9617147, -96.8255502), "The Lion & Crown", "5001 Addison Cir Addison TX 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9524276, -96.8384397), "The Londoner - Addison", "14930 Midway Rd Addison TX 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9619055, -96.8233469), "The Mucky Duck Bar", "5064 Addison CircleAddison TX 75001 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.882477, -96.895172), "Beamers Nightclub", "2443 Walnut Hill LnDallas TX 75229 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.106713, -96.808455), "Down Under Pub & Grub", "3231 Preston RdFrisco TX 75034 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0021361, -96.9627546), "Frankies - Lewisville", "2516 S. Stemmons FrwyLewisville TX 75067 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.012686, -96.992076), "Hat Tricks", "101 E Corporate DrLewisville TX 76116 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8506004, -96.8190164), "Inwood Lounge", "5458 W Lovers Ln Dallas TX 75209 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.850113, -96.8215807), "Inwood Tavern", "7717 Inwood Rd Dallas TX 75209 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.89419, -96.896751), "Jaguars Dallas", "11327 Reeder RdDallas TX 75229 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9888868, -96.8024582), "Main Stage Live Bar & Venue", "17449 Preston Rd Dallas TX.Dallas TX 75252 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.989509, -96.831208), "Malarkeys Tavern", "4460 Trinity Mills Rd #100Dallas TX 75287 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.84164, -96.771991), "The Barley House", "5612 SMU BlvdDallas TX 75206 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.1031361, -96.8130147), "The Irish Rover Pub & Restaurant", "8250 Gaylord PkwyFrisco TX 75034 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.1031361, -96.8130147), "TopGolf - Allen", "1500 Andrews PkwyAllen TX 75002 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.1031361, -96.8130147), "XTC Cabaret Dallas", "8550 North Stemmons FreewayDallas TX 75247 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.9618763, -96.9960925), "Yucatan Beach Club", "1850 East Beltline RoadCoppell TX 75019 " , R.drawable.dance));






    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }
}