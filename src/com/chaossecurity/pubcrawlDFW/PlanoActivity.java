package com.chaossecurity.pubcrawlDFW;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.chaossecurity.pubcrawlDFW.model.Bars;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lk on 11/18/14.
 */
public class PlanoActivity  extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : "  + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address+ ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(33.0307395, -96.7091317), 13f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        mClusterManager.addItem(new Bars(position(33.0788973, -96.8233849), "Blue Martini - Plano", "7301 Lone Star DrivePlano TX  75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0761045, -96.8219749), "Cru, A Wine Bar - Plano", "7201 Bishop Rd Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0189738, -96.6989617), "Last Chance Saloon", "1410 K Ave Plano TX 75035 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0758046, -96.8227172), "Ringos Pub", "5865 Kincaid Rd Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(41.6895524, -89.93653), "Sambuca 360", "7200 Bishop Rd #270 Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0758046, -96.8227172), "Scruffy Duffies", "5865 Kincaid Road Suite E-8Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.1060813, -96.8183152), "TailGaters", "7777 Warren ParkwayFrisco TX 75035 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0307395, -96.7091317), "The Allen Wickers", "2301 N Central Expy Ste 195 Plano TX 75075 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0418624, -96.751797), "The End Zone", "3033 W Parker Rd Plano TX 75023 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.019468, -96.700341), "The Fillmore Pub", "1004 E 15th St Plano TX 75074 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.117416, -96.8407788), "The Frisco Bar", "4851 Legacy Drive Suite 504Frisco TX 75034 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0767166, -96.8216647), "The Ginger Man - Plano", "7205 Bishop Rd Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.0899398, -96.8048306), "The Holy Grail Pub", "8240 Preston Rd Plano TX 75024 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.019978, -96.700434), "Vickery Park - Plano", "1011 E 15th St Plano TX 75074 " , R.drawable.dance));





    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }}