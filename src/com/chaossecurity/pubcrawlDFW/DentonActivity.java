package com.chaossecurity.pubcrawlDFW;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.utils.demo.model.Bars;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lk on 11/16/14.
 */
public class DentonActivity extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : "  + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address+ ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(33.2125982, -97.127048), 15f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        // http://www.flickr.com/photos/nypl/3111525394/
        mClusterManager.addItem(new Bars(position(33.212585, -97.127123), "Rubber Gloves Rehearsal Studios", "411 E Sycamore St Denton TX 76205 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(33.2157559, -97.1345881), "The Lab", "218 W Oak St Denton TX 76201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.21426, -97.133343), "The Loophole", "119 W Hickory St Denton TX 76201 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(33.2143567, -97.1453941), "Lucky Lous", "1207 W Hickory St Denton TX 76201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.2194827, -97.1321698), " The Greenhouse", "600 N Locust St Denton TX 76201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.2157479, -97.1315028), "East Side", "117 E Oak St Denton TX 76201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(33.2141726, -97.1299834), "Dans Silverleaf", "103 Industrial St Denton TX 76201 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(33.2153177, -97.1297556), "Oak Street Draft House", "308 E Oak St Denton TX 76201 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(33.2125982, -97.127048), "Rubber Gloves Rehearsal Studios", "411 E Sycamore St Denton TX 76205 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(33.2154181, -97.1321699), "Paschall Bar", "122 N Locust St Denton Texas 76201 " , R.drawable.dance));




    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }
}