package com.chaossecurity.pubcrawlDFW;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.utils.demo.model.Bars;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by spencer on 11/12/14.
 */
public class DallasActivity extends BaseActivity implements ClusterManager.OnClusterClickListener<Bars>, ClusterManager.OnClusterInfoWindowClickListener<Bars>, ClusterManager.OnClusterItemClickListener<Bars>, ClusterManager.OnClusterItemInfoWindowClickListener<Bars> {
    private ClusterManager<Bars> mClusterManager;
    //private Random mRandom = new Random(1984);

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class BarsRenderer extends DefaultClusterRenderer<Bars> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private String lineSep = System.getProperty("line.separator");

        public BarsRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Bars bars, MarkerOptions markerOptions) {
            // Draw a single person.
            mImageView.setImageResource(bars.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(bars.name + lineSep + " : "  + bars.address);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Bars> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Bars p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Bars> cluster) {
        String lineSep = System.getProperty("line.separator");
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, String.format("%d (including %s%s%s)", cluster.getSize(), firstName, lineSep, address), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Bars> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        String address = cluster.getItems().iterator().next().address;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + address+ ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onClusterItemClick(Bars item) {
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Bars item) {
    }

    @Override
    protected void startDemo() {
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.781652, -96.798772), 15f));

        mClusterManager = new ClusterManager<Bars>(this, getMap());
        mClusterManager.setRenderer(new BarsRenderer());
        getMap().setOnCameraChangeListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        // http://www.flickr.com/photos/nypl/3111525394/
        mClusterManager.addItem(new Bars(position(32.7804455, -96.8008252), "Carpe Diem Lounge", "1301 Main St Dallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.782288, -96.790428), "Cascade Ultra Lounge", "2210 Main StreetDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7802056, -96.8000556), "City Tavern - Dallas", "1402 Main StDallas TX 75202 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(32.7839985, -96.8057047), "Club Bliss", "1911 N. Griffin St Dallas TX 75334 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.780973, -96.792758), "Club Pure", "2026 Commerce St Dallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7849177, -96.7948409), "Draft Media Sports Lounge", "400 N Olive StDallas TX 75201 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(32.7844597, -96.7816515), "Eden Lounge", "2911 Main Street Dallas TX 75226 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7804455, -96.8008252), "Empire Rock Bar", "1301 Main St Dallas TX 75202 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(32.7805295, -96.8004828), "Erick Black", "1311 Main StDallas Texas 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7883746, -96.8090378), "Ghostbar", "2440 Victory Park LaneDallas TX 75219 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.785143, -96.797725), "Harwood 609", "609 N Harwood StDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7895073, -96.8091209), "Havana Social Club", "3030 Olive StDallas TX 75219 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.784987, -96.8082086), "House of Blues - Dallas", "2200 N Lamar St Dallas TX 75202 " , R.drawable.music));
        mClusterManager.addItem(new Bars(position(32.786767, -96.796555), "Luxx Nightclub", "723 N Pearl StreetDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7837936, -96.8066805), "Mambo Cafe", "2020 N Lamar St Dallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.780521, -96.798631), "Mantus", "1520 Main Street Suite BDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.78025, -96.800004), "OE Penguin", "1404 1/2 Main StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7802278, -96.8001977), "Plush", "1400 Main StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7805766, -96.7983196), "PM: A Nightlife Lounge", "1530 Main StDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7812356, -96.7975281), "Press Box Grill", "1623 Main StDallas TX 75201 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(32.786767, -96.796555), "Privado Nightclub", "723 Pearl StreetDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.78046, -96.801215), "Red Lights", "1217 Main StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7799284, -96.7998592), "Rodeo Bar & Grill", "1321 Commerce StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.8730585, -96.7678103), "Sherlocks Baker Street Pub & Grill - Dallas", "9100 N Central ExpyDallas TX  75231 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.78046, -96.801215), "Skye Bar", "1217 Main StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7816357, -96.7982204), "Spread Eagle Saloon", "1608 Elm StreetDallas TX 75201 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7803001, -96.8007488), "Ten Sports Grill", "1302 Main StDallas TX 75202 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(32.78025, -96.800004), "The Chesterfield", "1404 Main StDallas TX 75202 " , R.drawable.pubs));
        mClusterManager.addItem(new Bars(position(32.7883746, -96.8090378), "The Living Room Bar", "2440 Victory Park Ln Dallas TX 75219 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7754699, -96.804297), "The Owners Box", "555 S LamarDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.78091, -96.803701), "Thrive Nightclub", "1015 Elm StreetDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7770521, -96.8011976), "w xyz bar", "1033 Young St Dallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.782307, -96.806312), "West End Pub", "1801 N Lamar StDallas TX 75202 " , R.drawable.dance));
        mClusterManager.addItem(new Bars(position(32.7838675, -96.809563), "Zouk", "703 McKinney Ave Dallas TX 75219 " , R.drawable.dance));





    }

    private LatLng position(double min, double max) {
        return new LatLng(min, max);
    }
}